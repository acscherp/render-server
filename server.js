const childProcess = require('child_process')
const os = require('os')
const fs = require('fs')
const path = require('path')
const md5 = require('md5')
const cdp = require('chrome-remote-interface')

function performCrop(data) {
  const fn = '/tmp/render' + (Math.random() * 1000000) + '.png'
  fs.writeFileSync(fn, data)
  childProcess.execSync('mogrify -bordercolor white -border 1x1 -trim +repage ' + fn)
  return fs.readFileSync(fn)
}

function performBorder(data) {
  const fn = '/tmp/render' + (Math.random() * 1000000) + '.png'
  fs.writeFileSync(fn, data)
  childProcess.execSync('mogrify -bordercolor white -border 20x20 ' + fn)
  return fs.readFileSync(fn)
}

function startChrome(size) {
  return childProcess.spawn(
    path.resolve('/usr/bin/google-chrome'),
    [
      '--disable-gpu',
      '--homedir=/tmp',
      '--headless',
      `--window-size=${size}`,
      '--remote-debugging-port=9222',
    ],
    {
      cwd: os.tmpdir(),
      shell: true,
    }
  )
}

function sleep(ms) {
  return new Promise(res => {
    setTimeout(res, ms)
  })
}

async function doInNewContext(action) {
  const client = await cdp()
  try {
    return await action(client)
  }
  finally {
    await client.close()
  }
}

async function screenShot(client, url, delay) {
  const { Page } = client

  await Page.enable()
  await Page.navigate({ url })
  await Page.loadEventFired()
  if (delay) {
    await sleep(delay)
  }
  const { data } = await Page.captureScreenshot()
  return Buffer.from(data, 'base64')
}

const express = require('express')
const app = express()

app.get('/', async function(req, res) {
  if (!req.query.u || !req.query.s) {
    res.end('Missing parameters')
    return
  }
  const delay = req.query.d ? parseInt(req.query.d, 10) : 0

  const filename = '/tmp/out-' + md5(JSON.stringify(req.query)) + '.png'

  if (fs.existsSync(filename)) {

    res.header("Content-Type", "image/png");
    res.header("Connection", "close");
    res.sendFile(filename);

  } else {

    try {
      startChrome(req.query.s)
      await sleep(2000)
      let data = await doInNewContext(c => screenShot(c, req.query.u, delay))
      if (req.query.crop === 'true') {
        data = performCrop(data)
      }
      if (req.query.border === 'true') {
        data = performBorder(data)
      }
      res.header("Content-Type", "image/png");
      res.header("Connection", "close");
      res.end(data, 'binary');

      childProcess.spawn('killall', ['chrome'])

      fs.writeFileSync(filename, data);

      console.log('ok!!')

    } catch (e) {
      console.error(e)
      res.send('Error')
    }
  }
})

app.listen(8080)
