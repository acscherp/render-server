const childProcess = require('child_process')
const os = require('os')
const path = require('path')
const cdp = require('chrome-remote-interface')
const get = require('got')
const LOADING_TIMEOUT = 15000
const STARTUP_TIMEOUT = 5000

function startChrome(size) {
  return childProcess.spawn(
    path.resolve('/usr/bin/google-chrome'),
    [
      '--disable-gpu',
      '--homedir=/tmp',
      '--headless',
      `--window-size=${size}`,
      '--remote-debugging-port=9222',
    ],
    {
      cwd: os.tmpdir(),
      shell: true,
    }
  )
}

module.exports = { startChrome }
